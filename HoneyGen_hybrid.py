import fasttext
from random import random

def chaffing_by_model(real_password,k,l):
    """
    This function is used in the chaffing with a hybrid model method.
    :param real_password:
    :param k:
    :param l:
    :return:
    """

    # load honeywords generation model (FastText)
    print("Loading FastText word embeddings model...")
    model = fasttext.load_model("model_trained_on_rockyou_500_epochs.bin")
    print("Word embeddings model loaded.")
    print()

    # read target dataset
    print("Producing l honeywords with a model for each real password found in the target dataset...")
    #list hosting the generated honeywords
    honeywords=[]
    honeywords.append(real_password)
    temp = model.get_nearest_neighbors(real_password,k=l-1)
    for element in temp:
        honeywords.append(element[1])

    #after having l honeywords (including the real password) return the list
    return honeywords


def chafffing_by_tweaking_only(real_password,k):
    symbols = ['!', '#', '$', '%', '&', '"', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?',
               '@', '[', '\\', ']', '^', '_', '`', '{', '|', '}', '~', "'"]

    honeywords={}
    honeywords[real_password]=1
    duplicates_limit=0
    replaced_symbols=[0 for i in range(len(real_password))]
    p = 0.3
    f = 0.03
    q = 0.05
    while len(honeywords)<k:
        temp=""
        for i in range(len(real_password)):
            if real_password[i]>='a' and real_password[i]<='z':
                if random()<=f:
                    temp+=real_password[i].upper()
                else:
                    temp+=real_password[i]
            elif real_password[i]>='A' and real_password[i]<='Z':
                if random()<=p:
                    temp+=real_password[i].lower()
                else:
                    temp+=real_password[i]
            elif real_password[i]>='0' and real_password[i]<='9':
                if random()<=q:
                    #temp+=str(randint(0, 9))
                    temp+=str(int(random()*10))
                else:
                    temp+=real_password[i]
            elif real_password[i] in symbols:
                if replaced_symbols[i]=='n':
                    temp+=real_password[i]
                    continue
                if replaced_symbols[i]==0:
                    if random()<=p:
                        #index_symbol =randint(0,31)
                        index_symbol =int(len(symbols)*random())
                        temp+=symbols[index_symbol]
                        for j in range(i+1,len(real_password)):
                            if real_password[j]==real_password[i]:
                                replaced_symbols[j] = symbols[index_symbol]
                    else:
                        for j in range(i+1,len(real_password)):
                            if real_password[j]==real_password[i]:
                                replaced_symbols[j] = 'n'
                        temp+=real_password[i]
                else:
                    temp+=replaced_symbols[i]

        if temp in honeywords:
            duplicates_limit+=1
            if duplicates_limit % 4 == 0:
                p += 0.1
                q += 0.1
                f += 0.1
        else:
            honeywords[temp]=1

    #for key,value in honeywords.items():
    #    print(key)
    #exit(0)
    return honeywords


def chaffing_by_tweaking(honeywords,k,l):
    """
    This function is the 2nd stage (tweaking) in the hybrid method.
    :param honeywords: A list containing the produced by chaffing with a model HGT.
    :param k: Number of sweetwords.
    :param l: Number of sweetowrds to be produce by model (including the real password).
    :return: A list of produced sweetwords.
    """

    #how many chaffed with tweaking words per honeyword
    r = k/l

    new_honeywords=[]
    for i in honeywords:
        honeytemps = chafffing_by_tweaking_only(i, r)
        for z in honeytemps:
            new_honeywords.append(z)

    return new_honeywords


def chaffing_with_a_hybrid_model(real_password,k,l):
    """
    This function creates k honeyowrds in total. The l parameter denotes the total number of honeywords to be returned
    by the model (including the real password). The rest of them (k-l) are produced with the chaffing by tweaking method. In particular, it tweaks the
    honeywords produced by the chaffing by model method.
    :param real_password: The real-password for which to produce the honeywords.
    :param k: Total sweetwords per user.
    :param l: The honeywords to be returned my the model (including the real-password)
    :return:
    """

    if k%l!=0:
        print("Wrong arguments given!!! Specify other k and l values.")
        exit(0)

    #issue chaffing by model technique
    honeywords = chaffing_by_model(real_password,k,l)

    #issue chaffing by tweaking technique
    honeywords = chaffing_by_tweaking(honeywords,k,l)

    #honeywords list has the k sweetwords for the given password you can make whatever you want with them.

    print(honeywords)
    print(len(honeywords))

    print("Honeywords produced.")


#execute program
if __name__ == '__main__':
    real_password = input("Give the password for producing honeywords: ")
    real_password = real_password.replace("\n", "")

    k = 20  # number of sweetwords
    chaffing_with_a_hybrid_model(real_password, k, 10)

