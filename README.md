# README #

Implementation of HoneyGen's chaffing-with-a-hybrid-model HGT for producing honeywords.

### Dependecies ###

* fasttext 0.9.2
* Python 3.6
* scipy 1.4.1
* numpy 1.18.4
* matplotlib 3.2.1

### How do i run it? ###

* Training of the password embeddigns model (issue only once!)
	- Step 1: `python3 FastText.py` and wait until the training of the model finishes (size of the produced model ~12GB).

* Produce 20 honeywords for a given password:
	- Step 1: `python3 HoneyGen_hybrid.py` and follow the on-screen instructions.

### Reproduce the paper's experiments ###

* Run `python3 plot_graphs_experiments.py` and select the preferred number.

### Who do I talk to? ###

* Having problems? Email me. adiony01@cs.ucy.ac.cy

### Citing this work ###

If you use this repository for academic research, you are highly encouraged (though not required) to cite our paper:

```
@inproceedings{dionysiou2021honeygen,
  title={HoneyGen: Generating Honeywords Using Representation Learning},
  author={Dionysiou, Antreas and Vassiliades, Vassilis and Athanasopoulos, Elias},
  booktitle={Proceedings of the 2021 ACM Asia Conference on Computer and Communications Security},
  pages={265--279},
  year={2021}
}
```
